#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import json
import subprocess
import shlex

outputs = []

def create_still_image(image, duration, output):
    cmd = ["ffmpeg",
           "-loop", "1", "-i", image,
           "-t", str(duration), "-c:v", "libx264", "-preset", "ultrafast", "-pix_fmt", "yuv420p",
           output]

    outputs.append(output)
    subprocess.call(cmd)

def create_transition(image_from, image_to, output):
    cmd = ["ffmpeg",
           "-loop", "1","-i", image_from,
           "-loop", "1", "-i", image_to,
           "-filter_complex", "[1:v][0:v]blend=all_expr='A*(if(gte(T,3),1,T/3))+B*(1-(if(gte(T,3),1,T/3)))'",
           "-t", "3", "-c:v", "libx264", "-preset", "ultrafast", "-pix_fmt", "yuv420p",
           output]

    outputs.append(output)
    subprocess.call(cmd)

def create_slideshow(output):

    with open('.tmp/concat.txt', "w+") as outf:
        outf.writelines(map(lambda x: "file '%s'\n" % os.path.abspath(x), outputs))
        outf.close()

    cmd = shlex.split("ffmpeg -f concat -i .tmp/concat.txt -f mov -codec copy -shortest %s" % sys.argv[1])
    subprocess.call(cmd)

def main(argv):
    slides = json.load(sys.stdin)
    last_slide = slides[-1]

    transition_duration = 3

    for index, slide in enumerate(slides):
        
        slide_basename = os.path.basename(slide['path']);
        slide_still_duration = slide['duration']

        if slide != last_slide:
            slide_still_duration -= transition_duration

        create_still_image(slide['path'], slide_still_duration, '.tmp/'+slide_basename+'.mov')

        if (slide != last_slide):
            next_slide = slides[index + 1]
            create_transition(slide['path'], next_slide['path'],
                              '.tmp/%s_to_%s.mov' % (os.path.basename(slide['path']),
                                                     os.path.basename(next_slide['path'])))

    create_slideshow(argv[1])

if __name__ == "__main__":
    main(sys.argv)
