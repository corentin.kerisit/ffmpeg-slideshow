A much better tool is available here: https://github.com/h2non/videoshow

# FFmpeg slideshow

This was a dummy script to generate a slideshow out of multiple images

## Usage


Require a JSON config file such config.json:

```json
[
  {
    "path": "PATH_TO/image.png",
    "duration": 384
  },
  {
    "path": "path_to/image2.png",
    "duration": 324
  }
]
```

Then generate the slideshow:

```shell
cat config.json | python make_slideshow.py slideshow.mp4
```
